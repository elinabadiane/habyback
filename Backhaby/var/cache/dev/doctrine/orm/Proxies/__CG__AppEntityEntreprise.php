<?php

namespace Proxies\__CG__\App\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Entreprise extends \App\Entity\Entreprise implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'id', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'nomentrep', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'logo', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'adressentrep', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'descriptionentrep', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'siteinternet', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'users', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'entrsecteur'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'id', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'nomentrep', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'logo', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'adressentrep', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'descriptionentrep', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'siteinternet', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'users', '' . "\0" . 'App\\Entity\\Entreprise' . "\0" . 'entrsecteur'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Entreprise $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId(): ?int
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getNomentrep(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNomentrep', []);

        return parent::getNomentrep();
    }

    /**
     * {@inheritDoc}
     */
    public function setNomentrep(string $nomentrep): \App\Entity\Entreprise
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNomentrep', [$nomentrep]);

        return parent::setNomentrep($nomentrep);
    }

    /**
     * {@inheritDoc}
     */
    public function getLogo(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLogo', []);

        return parent::getLogo();
    }

    /**
     * {@inheritDoc}
     */
    public function setLogo(string $logo): \App\Entity\Entreprise
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLogo', [$logo]);

        return parent::setLogo($logo);
    }

    /**
     * {@inheritDoc}
     */
    public function getAdressentrep(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAdressentrep', []);

        return parent::getAdressentrep();
    }

    /**
     * {@inheritDoc}
     */
    public function setAdressentrep(string $adressentrep): \App\Entity\Entreprise
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAdressentrep', [$adressentrep]);

        return parent::setAdressentrep($adressentrep);
    }

    /**
     * {@inheritDoc}
     */
    public function getDescriptionentrep(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDescriptionentrep', []);

        return parent::getDescriptionentrep();
    }

    /**
     * {@inheritDoc}
     */
    public function setDescriptionentrep(string $descriptionentrep): \App\Entity\Entreprise
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDescriptionentrep', [$descriptionentrep]);

        return parent::setDescriptionentrep($descriptionentrep);
    }

    /**
     * {@inheritDoc}
     */
    public function getSiteinternet(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSiteinternet', []);

        return parent::getSiteinternet();
    }

    /**
     * {@inheritDoc}
     */
    public function setSiteinternet(string $siteinternet): \App\Entity\Entreprise
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSiteinternet', [$siteinternet]);

        return parent::setSiteinternet($siteinternet);
    }

    /**
     * {@inheritDoc}
     */
    public function getUsers(): \Doctrine\Common\Collections\Collection
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUsers', []);

        return parent::getUsers();
    }

    /**
     * {@inheritDoc}
     */
    public function addUser(\App\Entity\User $user): \App\Entity\Entreprise
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addUser', [$user]);

        return parent::addUser($user);
    }

    /**
     * {@inheritDoc}
     */
    public function removeUser(\App\Entity\User $user): \App\Entity\Entreprise
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeUser', [$user]);

        return parent::removeUser($user);
    }

    /**
     * {@inheritDoc}
     */
    public function getEntrsecteur(): \Doctrine\Common\Collections\Collection
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEntrsecteur', []);

        return parent::getEntrsecteur();
    }

    /**
     * {@inheritDoc}
     */
    public function addEntrsecteur(\App\Entity\Secteur $entrsecteur): \App\Entity\Entreprise
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addEntrsecteur', [$entrsecteur]);

        return parent::addEntrsecteur($entrsecteur);
    }

    /**
     * {@inheritDoc}
     */
    public function removeEntrsecteur(\App\Entity\Secteur $entrsecteur): \App\Entity\Entreprise
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeEntrsecteur', [$entrsecteur]);

        return parent::removeEntrsecteur($entrsecteur);
    }

}
